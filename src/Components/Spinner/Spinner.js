import React from "react";
import { useSelector } from "react-redux";
import { ScaleLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerReducer);
  return isLoading ? (
    <div className="h-screen w-screen fixed left-0 top-0 flex justify-center items-center z-50">
      <ScaleLoader size={50} color="#ff006e" />
    </div>
  ) : (
    <></>
  );
}
